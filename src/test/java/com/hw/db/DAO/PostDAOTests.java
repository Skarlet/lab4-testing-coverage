package com.hw.db.DAO;

import com.hw.db.models.Post;
import com.hw.db.models.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;

import static org.mockito.Mockito.*;

public class PostDAOTests {

    @Test
    @DisplayName("set Post test #1")
    void setPostTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO post = new PostDAO(mockJdbc);
        Post testPost = new Post();
        PostDAO.setPost(1, testPost);
        verify(mockJdbc).queryForObject(Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt());
        verifyNoMoreInteractions(mockJdbc);
    }

    @Test
    @DisplayName("set Post test #2")
    void setPostTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO post = new PostDAO(mockJdbc);

        Post testPost = new Post();
        Post testPostEx = new Post();
        testPostEx.setAuthor("");
        testPost.setAuthor("test");
        when(PostDAO.getPost(1)).thenReturn(testPostEx);
        PostDAO.setPost(1, testPost);
        verify(mockJdbc).queryForObject(Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt());
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
                Mockito.eq("test"), Mockito.eq(1));
    }

    @Test
    @DisplayName("set Post test #3")
    void setPostTest3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO post = new PostDAO(mockJdbc);

        Post testPost = new Post();
        Post testPostEx = new Post();
        testPostEx.setMessage("");
        testPost.setMessage("test");
        when(PostDAO.getPost(1)).thenReturn(testPostEx);
        PostDAO.setPost(1, testPost);
        verify(mockJdbc).queryForObject(Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt());
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
                Mockito.eq("test"), Mockito.eq(1));
    }

    @Test
    @DisplayName("set Post test #4")
    void setPostTest4() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO post = new PostDAO(mockJdbc);

        Post testPost = new Post();
        Post testPostEx = new Post();
        Timestamp ts = Timestamp.valueOf("2020-01-03 00:00:00");
        testPostEx.setCreated(Timestamp.valueOf("2020-01-02 00:00:00"));
        testPost.setCreated(ts);
        when(PostDAO.getPost(1)).thenReturn(testPostEx);
        PostDAO.setPost(1, testPost);
        verify(mockJdbc).queryForObject(Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt());
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq(ts), Mockito.eq(1));
    }

    @Test
    @DisplayName("set Post test #5")
    void setPostTest5() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO post = new PostDAO(mockJdbc);
        Post testPost = new Post();
        Post testPostEx = new Post();
        testPostEx.setAuthor("");
        testPost.setAuthor("testAuth");
        testPostEx.setMessage("");
        testPost.setMessage("test");
        when(PostDAO.getPost(1)).thenReturn(testPostEx);
        PostDAO.setPost(1, testPost);
        verify(mockJdbc).queryForObject(Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt());
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"),
                Mockito.eq("testAuth"), Mockito.eq("test"), Mockito.eq(1));
    }

    @Test
    @DisplayName("set Post test #6")
    void setPostTest6() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO post = new PostDAO(mockJdbc);
        Post testPost = new Post();
        Post testPostEx = new Post();
        testPostEx.setAuthor("");
        testPost.setAuthor("testAuth");
        Timestamp ts = Timestamp.valueOf("2020-01-03 00:00:00");
        testPostEx.setCreated(Timestamp.valueOf("2020-01-02 00:00:00"));
        testPost.setCreated(ts);
        when(PostDAO.getPost(1)).thenReturn(testPostEx);
        PostDAO.setPost(1, testPost);
        verify(mockJdbc).queryForObject(Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt());
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , " +
                        "isEdited=true WHERE id=?;"),
                Mockito.eq("testAuth"), Mockito.eq(ts), Mockito.eq(1));
    }

    @Test
    @DisplayName("set Post test #7")
    void setPostTest7() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO post = new PostDAO(mockJdbc);
        Post testPost = new Post();
        Post testPostEx = new Post();
        testPostEx.setMessage("");
        testPost.setMessage("test");
        Timestamp ts = Timestamp.valueOf("2020-01-03 00:00:00");
        testPostEx.setCreated(Timestamp.valueOf("2020-01-02 00:00:00"));
        testPost.setCreated(ts);
        when(PostDAO.getPost(1)).thenReturn(testPostEx);
        PostDAO.setPost(1, testPost);
        verify(mockJdbc).queryForObject(Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt());
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , " +
                        "isEdited=true WHERE id=?;"),
                Mockito.eq("test"), Mockito.eq(ts), Mockito.eq(1));
    }

    @Test
    @DisplayName("set Post test #8")
    void setPostTest8() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO post = new PostDAO(mockJdbc);
        Post testPost = new Post();
        Post testPostEx = new Post();
        testPostEx.setMessage("");
        testPost.setMessage("test");
        testPostEx.setAuthor("");
        testPost.setAuthor("testAuth");
        Timestamp ts = Timestamp.valueOf("2020-01-03 00:00:00");
        testPostEx.setCreated(Timestamp.valueOf("2020-01-02 00:00:00"));
        testPost.setCreated(ts);
        when(PostDAO.getPost(1)).thenReturn(testPostEx);
        PostDAO.setPost(1, testPost);
        verify(mockJdbc).queryForObject(Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt());
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , " +
                        "isEdited=true WHERE id=?;"), Mockito.eq("testAuth"),
                Mockito.eq("test"), Mockito.eq(ts), Mockito.eq(1));
    }
}
