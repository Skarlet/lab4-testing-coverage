package com.hw.db.DAO;

import com.hw.db.models.Post;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.*;

public class ThreadDAOTests {

    @Test
    @DisplayName("thread tree sort test #1")
    void setPostTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ThreadDAO threadDAO = new ThreadDAO(mockJdbc);

        ThreadDAO.treeSort(1, 2, 3, true);

        verify(mockJdbc).query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch " +
                        " FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"), Mockito.any(PostDAO.PostMapper.class),
                Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt());
    }
    @Test
    @DisplayName("thread tree sort test #2")
    void treeSortTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ThreadDAO threadDAO = new ThreadDAO(mockJdbc);

        ThreadDAO.treeSort(1, 2, 3, false);

        verify(mockJdbc).query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch " +
                        " FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"), Mockito.any(PostDAO.PostMapper.class),
                Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt());
    }
}
