package com.hw.db.DAO;

import com.hw.db.models.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.w3c.dom.UserDataHandler;

import static org.mockito.Mockito.*;

public class UserDAOTests {
    @Test
    @DisplayName("User change data test #1")
    void ChangeTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO user = new UserDAO(mockJdbc);
        UserDAO.Change(new User("test", "mail_test", "fullname_test", "about_test"));
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET " +
                        " email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("mail_test"), Mockito.eq("fullname_test"), Mockito.eq("about_test"),
                Mockito.eq("test") );
    }

    @Test
    @DisplayName("User change data test #2")
    void ChangeTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO user = new UserDAO(mockJdbc);
        UserDAO.Change(new User("test", "mail_test", "fullname_test", null));
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET " +
                        " email=? , fullname=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("mail_test"), Mockito.eq("fullname_test"), Mockito.eq("test") );
    }

    @Test
    @DisplayName("User change data test #3")
    void ChangeTest3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO user = new UserDAO(mockJdbc);
        UserDAO.Change(new User("test", "mail_test", null, "about_test"));
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET " +
                        " email=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("mail_test"), Mockito.eq("about_test"), Mockito.eq("test") );
    }

    @Test
    @DisplayName("User change data test #4")
    void ChangeTest4() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO user = new UserDAO(mockJdbc);
        UserDAO.Change(new User("test", "mail_test", null, null));
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET " +
                        " email=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("mail_test"), Mockito.eq("test") );
    }

    @Test
    @DisplayName("User change data test #5")
    void ChangeTest5() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO user = new UserDAO(mockJdbc);
        UserDAO.Change(new User("test", null, "fullname_test", "about_test"));
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET " +
                        " fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                 Mockito.eq("fullname_test"), Mockito.eq("about_test"), Mockito.eq("test") );
    }

    @Test
    @DisplayName("User change data test #6")
    void ChangeTest6() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO user = new UserDAO(mockJdbc);
        UserDAO.Change(new User("test", null, "fullname_test", null));
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET " +
                        " fullname=?  WHERE nickname=?::CITEXT;"),
                 Mockito.eq("fullname_test"), Mockito.eq("test") );
    }

    @Test
    @DisplayName("User change data test #7")
    void ChangeTest7() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO user = new UserDAO(mockJdbc);
        UserDAO.Change(new User("test", null, null, "about_test"));
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET " +
                        " about=?  WHERE nickname=?::CITEXT;"),
                 Mockito.eq("about_test"), Mockito.eq("test") );
    }

    @Test
    @DisplayName("User change data test #8")
    void ChangeTest8() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO user = new UserDAO(mockJdbc);
        UserDAO.Change(new User("test", null, null, null));
        verifyNoInteractions(mockJdbc);
    }
}
