package com.hw.db.DAO;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Array;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ForumDAOTests {

    @Test
    @DisplayName("User gets list of threads test #1")
    void ThreadListTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ThreadDAO.ThreadMapper THREAD_MAPPER = new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug",null, null, null);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created;"),
                Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    @DisplayName("User gets list of user test #1")
    void UserListTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ForumDAO.UserList("slug",1, "01-02-2000", true);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users " +
                        "WHERE forum = (?)::citext AND nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("User gets list of user test #2")
    void UserListTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ForumDAO.UserList("slug",null, "01-02-2000", false);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users " +
                        "WHERE forum = (?)::citext AND nickname > (?)::citext ORDER BY nickname;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("User gets list of user test #3")
    void UserListTest3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ForumDAO.UserList("slug",null, null, false);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users " +
                        "WHERE forum = (?)::citext ORDER BY nickname;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }
}
